package com.kaixeleron.mojang;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MojangApiManager {

    public MojangApiManager(JavaPlugin plugin) {

        this.plugin = plugin;

        gson = new Gson();

        cache = Collections.synchronizedMap(new HashMap<>());
        skins = Collections.synchronizedMap(new HashMap<>());
        skinSignatures = Collections.synchronizedMap(new HashMap<>());

    }

    private final JavaPlugin plugin;

    private final Gson gson;

    private final Map<String, UUID> cache;

    private final Map<String, String> skins, skinSignatures;

    public UUID getUUID(final String name) throws IOException {

        UUID out;

        synchronized (cache) {

            out = cache.get(name);

        }

        if (out == null) try {

            HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://api.mojang.com/users/profiles/minecraft/%s", name)).openConnection();

            JsonObject data = gson.fromJson(new InputStreamReader(connection.getInputStream()), JsonObject.class);

            String nodash = data.get("id").getAsString();

            out = UUID.fromString(nodash.replaceFirst( "([0-9a-fA-F]{8})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]+)", "$1-$2-$3-$4-$5"));

            synchronized (cache) {

                cache.put(name, out);

            }

            new BukkitRunnable() {

                @Override
                public void run() {

                    synchronized (cache) {

                        cache.remove(name);

                    }

                }

            }.runTaskLaterAsynchronously(plugin, 72000L);

        } catch (MalformedURLException ignored) {}

        return out;
    }

    public GameProfile applySkin(final GameProfile target, final String name) throws IOException {

        boolean cached = false;

        synchronized (skins) {

            synchronized (skinSignatures) {

                if (skins.containsKey(name) && skinSignatures.containsKey(name)) {

                    target.getProperties().put("textures", new Property("textures", skins.get(name), skinSignatures.get(name)));

                    cached = true;

                }
            }
        }

        if (!cached) try {

            HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false", getUUID(name).toString().replace("-", ""))).openConnection();

            JsonObject data = gson.fromJson(new InputStreamReader(connection.getInputStream()), JsonObject.class);

            JsonObject properties = (JsonObject) data.getAsJsonArray("properties").get(0);

            String texture = properties.get("value").getAsString(), signature = properties.get("signature").getAsString();

            target.getProperties().put("textures", new Property("textures", texture, signature));

            synchronized (skins) {

                skins.put(name, texture);

            }

            synchronized (skinSignatures) {

                skinSignatures.put(name, texture);

            }

            new BukkitRunnable() {

                @Override
                public void run() {

                    synchronized (skins) {

                        skins.remove(name);

                    }

                    synchronized (skinSignatures) {

                        skinSignatures.remove(name);

                    }

                }

            }.runTaskLaterAsynchronously(plugin, 72000L);

        } catch (MalformedURLException ignored) {}

        return target;
    }
}
