package com.kaixeleron.disguise.listener;

import com.kaixeleron.disguise.engine.DisguiseManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveListener implements Listener {

    public LeaveListener(final DisguiseManager dm) {

        this.dm = dm;

    }

    private final DisguiseManager dm;

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        dm.undisguise(event.getPlayer());

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        dm.undisguise(event.getPlayer());

    }
}
