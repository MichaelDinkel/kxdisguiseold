package com.kaixeleron.disguise.listener;

import com.kaixeleron.disguise.engine.DisguiseManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements Listener {

    public MoveListener(final DisguiseManager dm) {

        this.dm = dm;

    }

    private final DisguiseManager dm;

    @EventHandler
    public void onMove(PlayerMoveEvent event) {

        dm.updatePosition(event.getPlayer());

    }
}
