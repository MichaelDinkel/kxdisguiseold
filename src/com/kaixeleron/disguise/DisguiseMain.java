package com.kaixeleron.disguise;

import com.kaixeleron.disguise.command.CommandDisguise;
import com.kaixeleron.disguise.command.CommandDisguiseList;
import com.kaixeleron.disguise.command.CommandUndisguise;
import com.kaixeleron.disguise.engine.DisguiseManager;
import com.kaixeleron.disguise.listener.LeaveListener;
import com.kaixeleron.disguise.listener.MoveListener;
import com.kaixeleron.mojang.MojangApiManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

public class DisguiseMain extends JavaPlugin {

    private DisguiseManager dm;

    @Override
    public void onEnable() {

        dm = null;

        try {

            dm = new DisguiseManager(this, new MojangApiManager(this));

        } catch (ClassNotFoundException e) {

            getLogger().log(Level.SEVERE, "An adapter compatible with this server's Minecraft version was " +
                    "not found.");
            setEnabled(false);

            return;

        } catch (NoSuchMethodException | InvocationTargetException
                | IllegalAccessException | InstantiationException ignored) {}

        getCommand("disguise").setExecutor(new CommandDisguise(dm));
        getCommand("undisguise").setExecutor(new CommandUndisguise(dm));
        getCommand("disguiselist").setExecutor(new CommandDisguiseList(dm));

        getServer().getPluginManager().registerEvents(new LeaveListener(dm), this);
        getServer().getPluginManager().registerEvents(new MoveListener(dm), this);

    }

    @Override
    public void onDisable() {

        if (dm != null) dm.undisguiseAll();

    }
}
