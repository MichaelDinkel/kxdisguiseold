package com.kaixeleron.disguise.engine;

public enum Disguise {

    AREAEFFECTCLOUD("AreaEffectCloud", "kxdisguise.type.areaeffectcloud"),
    ARMORSTAND("ArmorStand", "kxdisguise.type.armorstand"),
    BAT("Bat", "kxdisguise.type.bat"),
    BLAZE("Blaze", "kxdisguise.type.blaze"),
    BOAT("Boat", "kxdisguise.type.boat"),
    CAVESPIDER("CaveSpider", "kxdisguise.type.cavespider"),
    CHICKEN("Chicken", "kxdisguise.type.chicken"),
    COD("Cod", "kxdisguise.type.cod"),
    COW("Cow", "kxdisguise.type.cow"),
    CREEPER("Creeper", "kxdisguise.type.creeper"),
    DOLPHIN("Dolphin", "kxdisguise.type.dolphin"),
    DONKEY("Donkey", "kxdisguise.type.donkey"),
    DROWNED("Drowned", "kxdisguise.type.drowned"),
    EGG("Egg", "kxdisguise.type.egg"),
    ELDERGUARDIAN("ElderGuardian", "kxdisguise.type.elderguardian"),
    ENDERMAN("Enderman", "kxdisguise.type.enderman"),
    ENDERMITE("Endermite", "kxdisguise.type.endermite"),
    ENDERCRYSTAL("EnderCrystal", "kxdisguise.type.endercrystal"),
    ENDERDRAGON("EnderDragon", "kxdisguise.type.enderdragon"),
    ENDERPEARL("EnderPearl", "kxdisguise.type.enderpearl"),
    ENDERSIGNAL("EnderSignal", "kxdisguise.type.endersignal"),
    EVOKER("Evoker", "kxdisguise.type.evoker"),
    GHAST("Ghast", "kxdisguise.type.ghast"),
    GIANT("Giant", "kxdisguise.type.giant"),
    GUARDIAN("Guardian", "kxdisguise.type.guardian"),
    HORSE("Horse", "kxdisguise.type.horse"),
    HUSK("Husk", "kxdisguise.type.husk"),
    ILLUSIONER("Illusioner", "kxdisguise.type.illusioner"),
    IRONGOLEM("IronGolem", "kxdisguise.type.irongolem"),
    ITEMFRAME("ItemFrame", "kxdisguise.type.itemframe"),
    LEASHHITCH("LeashHitch", "kxdisguise.type.leashhitch"),
    LLAMA("Llama", "kxdisguise.type.llama"),
    LLAMASPIT("LlamaSpit", "kxdisguise.type.llamaspit"),
    MAGMACUBE("MagmaCube", "kxdisguise.type.magmacube"),
    MINECART("Minecart", "kxdisguise.type.minecart"),
    MINECARTCHEST("MinecartChest", "kxdisguise.type.minecartchest"),
    MINECARTCOMMAND("MinecartCommand", "kxdisguise.type.minecartcommand"),
    MINECARTFURNACE("MinecartFurnace", "kxdisguise.type.minecartfurnace"),
    MINECARTHOPPER("MinecartHopper", "kxdisguise.type.minecarthopper"),
    MINECARTMOBSPAWNER("MinecartMobSpawner", "kxdisguise.type.minecartmobspawner"),
    MINECARTTNT("MinecartTnt", "kxdisguise.type.minecarttnt"),
    MULE("Mule", "kxdisguise.type.mule"),
    MUSHROOMCOW("MushroomCow", "kxdisguise.type.mushroomcow"),
    OCELOT("Ocelot", "kxdisguise.type.ocelot"),
    PAINTING("Painting", "kxdisguise.type.painting"),
    PARROT("Parrot", "kxdisguise.type.parrot"),
    PHANTOM("Phantom", "kxdisguise.type.phantom"),
    PIG("Pig", "kxdisguise.type.pig"),
    PIGZOMBIE("PigZombie", "kxdisguise.type.pigzombie"),
    PLAYER("Player", "kxdisguise.type.player"),
    POLARBEAR("PolarNear", "kxdisguise.type.polarbear"),
    PRIMEDTNT("PrimedTnt", "kxdisguise.type.primedtnt"),
    PUFFERFISH("Pufferfish", "kxdisguise.type.pufferfish"),
    RABBIT("Rabbit", "kxdisguise.type.rabbit"),
    SALMON("Salmon", "kxdisguise.type.salmon"),
    SHEEP("Sheep", "kxdisguise.type.sheep"),
    SHULKER("Shulker", "kxdisguise.type.shulker"),
    SHULKERBULLET("ShulkerBullet", "kxdisguise.type.shulkerbullet"),
    SILVERFISH("Silverfish", "kxdisguise.type.silverfish"),
    SKELETON("Skeleton", "kxdisguise.type.skeleton"),
    SKELETONHORSE("SkeletonJorse", "kxdisguise.type.skeletonhorse"),
    SLIME("Slime", "kxdisguise.type.slime"),
    SNOWBALL("Snowball", "kxdisguise.type.snowball"),
    SNOWMAN("Snowman", "kxdisguise.type.snowman"),
    SPIDER("Spider", "kxdisguise.type.spider"),
    SQUID("Squid", "kxdisguise.type.squid"),
    STRAY("Stray", "kxdisguise.type.stray"),
    TRIDENT("Trident", "kxdisguise.type.trident"),
    TROPICALFISH("TropicalFish", "kxdisguise.type.tropicalfish"),
    TURTLE("Turtle", "kxdisguise.type.turtle"),
    VEX("Vex", "kxdisguise.type.vex"),
    VILLAGER("Villager", "kxdisguise.type.villager"),
    VINDICATOR("Vindicator", "kxdisguise.type.vindicator"),
    WITCH("Witch", "kxdisguise.type.witch"),
    WITHER("Wither", "kxdisguise.type.wither"),
    WITHERSKELETON("WitherSkeleton", "kxdisguise.type.witherskeleton"),
    WITHERSKULL("WitherSkull", "kxdisguise.type.witherskull"),
    WOLF("Wolf", "kxdisguise.type.wolf"),
    ZOMBIE("Zombie", "kxdisguise.type.zombie"),
    ZOMBIEHORSE("ZombieHorse", "kxdisguise.type.zombiehorse"),
    ZOMBIEVILLAGER("ZombieVillager", "kxdisguise.type.zombievillager");

    private final String friendlyName, permission;

    Disguise(String friendlyName, String permission) {
        
        this.friendlyName = friendlyName;
        this.permission = permission;

    }
    
    public String getFriendlyName() {
        return friendlyName;
    }
    
    public String getPermission() {
        return permission;
    }
}
