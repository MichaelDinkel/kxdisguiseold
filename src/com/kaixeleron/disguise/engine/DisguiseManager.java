package com.kaixeleron.disguise.engine;

import com.kaixeleron.disguise.DisguiseMain;
import com.kaixeleron.disguise.engine.nms.NMSManager;
import com.kaixeleron.mojang.MojangApiManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class DisguiseManager {

    public DisguiseManager(final DisguiseMain m, final MojangApiManager apiManager) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        disguises = new HashMap<>();
        playerDisguiseNames = new HashMap<>();

        String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",")
                .split(",")[3];

        Class<?> nmsManagerClass = Class.forName("com.kaixeleron.disguise.engine.nms." + version + ".MinecraftHook");
        Constructor<?> nmsManagerConstructor = nmsManagerClass.getConstructor(JavaPlugin.class, MojangApiManager.class);
        manager = (NMSManager) nmsManagerConstructor.newInstance(m, apiManager);

    }

    private final Map<Player, Disguise> disguises;
    private final Map<Player, String> playerDisguiseNames;

    private final NMSManager manager;

    public void disguise(Player p, Disguise d) {

        manager.setDisguise(p, d);
        manager.hideWithTab(p);
        disguises.put(p, d);
        playerDisguiseNames.remove(p);

    }

    public void disguisePlayer(Player p, String name) {

        manager.setPlayerDisguise(p, name);
        manager.hideWithTab(p);
        disguises.put(p, Disguise.PLAYER);
        playerDisguiseNames.put(p, name);

    }

    public void undisguise(Player p) {

        manager.removeDisguise(p);
        manager.showHidden(p);
        disguises.remove(p);
        playerDisguiseNames.remove(p);

    }

    public void undisguiseAll() {

        for (Player p : disguises.keySet()) {

            manager.removeDisguise(p);

            p.sendMessage(ChatColor.AQUA + "You have been undisguised due to the plugin unloading.");

        }

        disguises.clear();
        playerDisguiseNames.clear();
    }

    public Disguise getDisguise(Player p) {
        return disguises.get(p);
    }

    public String getPlayerDisguiseName(Player p) {
        return playerDisguiseNames.get(p);
    }

    public Map<Player, Disguise> getDisguises() {
        return new HashMap<>(disguises);
    }

    public void updatePosition(Player p) {

        if (disguises.containsKey(p)) manager.updatePosition(p);

    }
}
