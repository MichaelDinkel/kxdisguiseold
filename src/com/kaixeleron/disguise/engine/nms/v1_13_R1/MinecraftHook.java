package com.kaixeleron.disguise.engine.nms.v1_13_R1;

import com.kaixeleron.disguise.engine.Disguise;
import com.kaixeleron.disguise.engine.nms.NMSManager;
import com.kaixeleron.mojang.MojangApiManager;
import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_13_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MinecraftHook implements NMSManager {

    public MinecraftHook(final JavaPlugin plugin, final MojangApiManager apiManager) {

        entities = new HashMap<>();

        this.plugin = plugin;

        this.apiManager = apiManager;

    }

    private final Map<Player, Entity> entities;

    private final JavaPlugin plugin;

    private final MojangApiManager apiManager;

    @Override
    public void setDisguise(Player p, Disguise d) {

        Location l = p.getLocation();

        World w = ((CraftWorld) p.getWorld()).getHandle();
        double x = l.getX(), y = l.getY(), z = l.getZ();

        Entity e;
        int deadType = 0;

        switch (d) {

            case AREAEFFECTCLOUD:
                e = new EntityAreaEffectCloud(w, x, y, z);
                deadType = 3;
                break;
            case ARMORSTAND:
                e = new EntityArmorStand(w, x, y ,z);
                deadType = 78;
                break;
            case BAT:
                e = new EntityBat(w);
                e.setPosition(x, y, z);
                break;
            case BLAZE:
                e = new EntityBlaze(w);
                e.setPosition(x, y, z);
                break;
            case BOAT:
                e = new EntityBoat(w, x, y, z);
                deadType = 1;
                break;
            case CAVESPIDER:
                e = new EntityCaveSpider(w);
                e.setPosition(x, y, z);
                break;
            case CHICKEN:
                e = new EntityChicken(w);
                e.setPosition(x, y, z);
                break;
            case COD:
                e = new EntityCod(w);
                e.setPosition(x, y, z);
                break;
            case COW:
                e = new EntityCow(w);
                e.setPosition(x, y, z);
                break;
            case CREEPER:
                e = new EntityCreeper(w);
                e.setPosition(x, y, z);
                break;
            case DOLPHIN:
                e = new EntityDolphin(w);
                e.setPosition(x, y, z);
                break;
            case DONKEY:
                e = new EntityHorseDonkey(w);
                e.setPosition(x, y, z);
                break;
            case DROWNED:
                e = new EntityDrowned(w);
                e.setPosition(x, y, z);
                break;
            case EGG:
                e = new EntityEgg(w, x, y, z);
                deadType = 62;
                break;
            case ELDERGUARDIAN:
                e = new EntityGuardianElder(w);
                e.setPosition(x, y, z);
                break;
            case ENDERMAN:
                e = new EntityEnderman(w);
                e.setPosition(x, y, z);
                break;
            case ENDERMITE:
                e = new EntityEndermite(w);
                e.setPosition(x, y, z);
                break;
            case ENDERCRYSTAL:
                e = new EntityEnderCrystal(w);
                e.setPosition(x, y, z);
                deadType = 51;
                break;
            case ENDERDRAGON:
                e = new EntityEnderDragon(w);
                e.setPosition(x, y, z);
                break;
            case ENDERPEARL:
                e = new EntityEnderPearl(w);
                e.setPosition(x, y, z);
                deadType = 65;
                break;
            case ENDERSIGNAL:
                e = new EntityEnderSignal(w, x, y, z);
                deadType = 72;
                break;
            case EVOKER:
                e = new EntityEvoker(w);
                e.setPosition(x, y, z);
                break;
            case GHAST:
                e = new EntityGhast(w);
                e.setPosition(x, y, z);
                break;
            case GIANT:
                e = new EntityGiantZombie(w);
                e.setPosition(x, y, z);
                break;
            case GUARDIAN:
                e = new EntityGuardian(w);
                e.setPosition(x, y, z);
                break;
            case HORSE:
                e = new EntityHorse(w);
                e.setPosition(x, y, z);
                break;
            case HUSK:
                e = new EntityZombieHusk(w);
                e.setPosition(x, y, z);
                break;
            case ILLUSIONER:
                e = new EntityIllagerIllusioner(w);
                e.setPosition(x, y, z);
                break;
            case IRONGOLEM:
                e = new EntityIronGolem(w);
                e.setPosition(x, y, z);
                break;
            case ITEMFRAME:
                e = new EntityItemFrame(w);
                e.setPosition(x, y, z);
                deadType = 71;
                break;
            case LEASHHITCH:
                e = new EntityLeash(w);
                e.setPosition(x, y, z);
                deadType = 77;
                break;
            case LLAMA:
                e = new EntityLlama(w);
                e.setPosition(x, y, z);
                break;
            case LLAMASPIT:
                e = new EntityLlamaSpit(w);
                e.setPosition(x, y, z);
                break;
            case MAGMACUBE:
                e = new EntityMagmaCube(w);
                e.setPosition(x, y, z);
                break;
            case MINECART:
                e = new EntityMinecartRideable(w, x, y, z);
                deadType = 10;
                break;
            case MINECARTCHEST:
                e = new EntityMinecartChest(w, x, y, z);
                deadType = 10;
                break;
            case MINECARTCOMMAND:
                e = new EntityMinecartCommandBlock(w, x, y, z);
                deadType = 10;
                break;
            case MINECARTFURNACE:
                e = new EntityMinecartFurnace(w, x, y, z);
                deadType = 10;
                break;
            case MINECARTHOPPER:
                e = new EntityMinecartHopper(w, x, y, z);
                deadType = 10;
                break;
            case MINECARTMOBSPAWNER:
                e = new EntityMinecartMobSpawner(w, x, y, z);
                deadType = 10;
                break;
            case MINECARTTNT:
                e = new EntityMinecartTNT(w, x, y, z);
                deadType = 10;
                break;
            case MULE:
                e = new EntityHorseMule(w);
                e.setPosition(x, y, z);
                break;
            case MUSHROOMCOW:
                e = new EntityMushroomCow(w);
                e.setPosition(x, y, z);
                break;
            case OCELOT:
                e = new EntityOcelot(w);
                e.setPosition(x, y, z);
                break;
            case PAINTING:
                e = new EntityPainting(w);
                e.setPosition(x, y, z);
                break;
            case PARROT:
                e = new EntityParrot(w);
                e.setPosition(x, y, z);
                break;
            case PHANTOM:
                e = new EntityPhantom(w);
                e.setPosition(x, y, z);
                break;
            case PIG:
                e = new EntityPig(w);
                e.setPosition(x, y, z);
                break;
            case PIGZOMBIE:
                e = new EntityPigZombie(w);
                e.setPosition(x, y, z);
                break;
            case POLARBEAR:
                e = new EntityPolarBear(w);
                e.setPosition(x, y, z);
                break;
            case PRIMEDTNT:
                e = new EntityTNTPrimed(w);
                e.setPosition(x, y, z);
                ((EntityTNTPrimed) e).setFuseTicks(Integer.MAX_VALUE);
                deadType = 50;
                break;
            case PUFFERFISH:
                e = new EntityPufferFish(w);
                e.setPosition(x, y, z);
                break;
            case RABBIT:
                e = new EntityRabbit(w);
                e.setPosition(x, y, z);
                break;
            case SALMON:
                e = new EntitySalmon(w);
                e.setPosition(x, y, z);
                break;
            case SHEEP:
                e = new EntitySheep(w);
                e.setPosition(x, y, z);
                break;
            case SHULKER:
                e = new EntityShulker(w);
                e.setPosition(x, y, z);
                break;
            case SHULKERBULLET:
                e = new EntityShulkerBullet(w);
                e.setPosition(x, y, z);
                deadType = 67;
                break;
            case SILVERFISH:
                e = new EntitySilverfish(w);
                e.setPosition(x, y, z);
                break;
            case SKELETON:
                e = new EntitySkeleton(w);
                e.setPosition(x, y, z);
                break;
            case SKELETONHORSE:
                e = new EntityHorseSkeleton(w);
                e.setPosition(x, y, z);
                break;
            case SLIME:
                e = new EntitySlime(w);
                e.setPosition(x, y, z);
                break;
            case SNOWBALL:
                e = new EntitySnowball(w, x, y, z);
                deadType = 61;
                break;
            case SNOWMAN:
                e = new EntitySnowman(w);
                e.setPosition(x, y, z);
                break;
            case SPIDER:
                e = new EntitySpider(w);
                e.setPosition(x, y, z);
                break;
            case SQUID:
                e = new EntitySquid(w);
                e.setPosition(x, y, z);
                break;
            case STRAY:
                e = new EntitySkeletonStray(w);
                e.setPosition(x, y, z);
                break;
            case TRIDENT:
                e = new EntityThrownTrident(w);
                e.setPosition(x, y, z);
                break;
            case TROPICALFISH:
                e = new EntityTropicalFish(w);
                e.setPosition(x, y, z);
                break;
            case TURTLE:
                e = new EntityTurtle(w);
                e.setPosition(x, y, z);
                break;
            case VEX:
                e = new EntityVex(w);
                e.setPosition(x, y, z);
                break;
            case VILLAGER:
                e = new EntityVillager(w);
                e.setPosition(x, y, z);
                break;
            case VINDICATOR:
                e = new EntityVindicator(w);
                e.setPosition(x, y, z);
                break;
            case WITCH:
                e = new EntityWitch(w);
                e.setPosition(x, y, z);
                break;
            case WITHER:
                e = new EntityWither(w);
                e.setPosition(x, y, z);
                break;
            case WITHERSKELETON:
                e = new EntitySkeletonWither(w);
                e.setPosition(x, y, z);
                break;
            case WITHERSKULL:
                e = new EntityWitherSkull(w);
                e.setPosition(x, y, z);
                deadType = 66;
                break;
            case WOLF:
                e = new EntityWolf(w);
                e.setPosition(x, y, z);
                break;
            case ZOMBIE:
                e = new EntityZombie(w);
                e.setPosition(x, y, z);
                break;
            case ZOMBIEHORSE:
                e = new EntityHorseZombie(w);
                e.setPosition(x, y, z);
                break;
            case ZOMBIEVILLAGER:
                e = new EntityZombieVillager(w);
                e.setPosition(x, y, z);
                break;
            default:
                return;
        }

        Packet packet;

        e.setCustomName(IChatBaseComponent.ChatSerializer.a(String.format("{\"text\":\"%s\"}", p.getName())));

        if (e instanceof EntityLiving) {

            ((EntityLiving) e).collides = false;

            packet = new PacketPlayOutSpawnEntityLiving((EntityLiving) e);

        } else packet = new PacketPlayOutSpawnEntity(e, deadType);

        entities.put(p, e);

        for (Player player : Bukkit.getOnlinePlayers()) {

            if (player.equals(p)) continue;

            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);

        }

        updatePosition(p);
    }

    @Override
    public void setPlayerDisguise(final Player p, final String name) {

        new BukkitRunnable() {

            @Override
            public void run() {

                try {

                    UUID id = apiManager.getUUID(name);

                    final GameProfile gp = apiManager.applySkin(new GameProfile(id, name), name);

                    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {

                        @SuppressWarnings("deprecation") EntityPlayer ep = new EntityPlayer(MinecraftServer.getServer(),
                                ((CraftPlayer) p).getHandle().getWorldServer(), gp, new PlayerInteractManager(((CraftPlayer) p)
                                .getHandle().getWorld()));

                        PacketPlayOutPlayerInfo infopack = null;
                        if (Bukkit.getPlayer(name) == null) infopack = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, ep);
                        PacketPlayOutNamedEntitySpawn packet = new PacketPlayOutNamedEntitySpawn(ep);

                        entities.put(p, ep);

                        for (Player player : Bukkit.getOnlinePlayers()) {

                            if (player.equals(p)) continue;

                            if (infopack != null) {

                                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(infopack);

                                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, ep)), 10L);

                            }

                            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);

                        }

                        updatePosition(p);

                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }.runTaskAsynchronously(plugin);

    }

    @Override
    public void removeDisguise(Player p) {

        Entity e = entities.get(p);

        PacketPlayOutPlayerInfo infopack = null;

        if (e instanceof EntityPlayer) infopack = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, (EntityPlayer) e);

        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(e.getId());

        for (Player player : Bukkit.getOnlinePlayers()) {

            if (infopack != null) ((CraftPlayer) player).getHandle().playerConnection.sendPacket(infopack);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);

        }

        entities.remove(p);

    }

    @Override
    public void updatePosition(Player p) {

        Entity e = entities.get(p);

        if (e == null) return;

        Location l = p.getLocation();

        if (!e.getWorld().equals(((CraftPlayer) p).getHandle().getWorld())) e.world = ((CraftPlayer) p).getHandle().getWorld();

        e.setPosition(l.getX(), l.getY(), l.getZ());
        PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport(e);
        PacketPlayOutEntityHeadRotation rotatepack = new PacketPlayOutEntityHeadRotation(e, (byte) (l.getYaw() + 90));
        PacketPlayOutEntity.PacketPlayOutEntityLook lookpack = new PacketPlayOutEntity.PacketPlayOutEntityLook(e.getId(),
                (byte) l.getYaw(), (byte) l.getPitch(), false);

        for (Player player : Bukkit.getOnlinePlayers()) {

            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(rotatepack);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(lookpack);

        }

    }

    @Override
    public void hideWithTab(Player hidden) {

        for (Player p : Bukkit.getOnlinePlayers()) {

            if (p.equals(hidden)) continue;

            EntityTracker tracker = ((WorldServer) ((CraftPlayer) p).getHandle().world).tracker;
            EntityPlayer other = ((CraftPlayer) hidden).getHandle();
            EntityTrackerEntry entry = tracker.trackedEntities.get(other.getId());

            if (entry != null) {
                entry.clear(((CraftPlayer) p).getHandle());
            }
        }
    }

    @Override
    public void showHidden(Player hidden) {

        for (Player p : Bukkit.getOnlinePlayers()) {

            EntityTracker tracker = ((WorldServer) ((CraftPlayer) p).getHandle().world).tracker;
            EntityPlayer other = ((CraftPlayer) hidden).getHandle();

            EntityTrackerEntry entry = tracker.trackedEntities.get(other.getId());

            if (entry != null && !entry.trackedPlayers.contains(((CraftPlayer) p).getHandle())) {
                entry.updatePlayer(((CraftPlayer) p).getHandle());
            }

        }
    }
}
