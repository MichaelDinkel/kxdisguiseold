package com.kaixeleron.disguise.engine.nms;

import com.kaixeleron.disguise.engine.Disguise;
import org.bukkit.entity.Player;

public interface NMSManager {

    void setDisguise(Player p, Disguise d);

    void setPlayerDisguise(Player p, String name);

    void removeDisguise(Player p);

    void updatePosition(Player p);

    void hideWithTab(Player hidden);

    void showHidden(Player hidden);

}
