package com.kaixeleron.disguise.command;

import com.kaixeleron.disguise.engine.Disguise;
import com.kaixeleron.disguise.engine.DisguiseManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class CommandDisguiseList implements CommandExecutor {

    public CommandDisguiseList(final DisguiseManager dm) {

        this.dm = dm;

    }

    private final DisguiseManager dm;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Map<Player, Disguise> disguises = dm.getDisguises();

        sender.sendMessage(ChatColor.AQUA + "Currently active disguises: ");

        for (Player p : disguises.keySet()) {

            String friendlyName = disguises.get(p).getFriendlyName();

            sender.sendMessage(p.getName() + ChatColor.AQUA + " as a" + ("aeiou".contains(friendlyName
                    .substring(0, 1).toLowerCase()) ? "n " : " ") + ChatColor.RESET + friendlyName);

        }

        return true;
    }
}
