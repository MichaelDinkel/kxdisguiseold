package com.kaixeleron.disguise.command;

import com.kaixeleron.disguise.engine.Disguise;
import com.kaixeleron.disguise.engine.DisguiseManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandDisguise implements CommandExecutor {

    public CommandDisguise(final DisguiseManager dm) {
        this.dm = dm;
    }

    private final DisguiseManager dm;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " <Type>" + (sender
                        .hasPermission("kxdisguise.command.disguise.other") ? " [Player]" : ""));

                Disguise current = dm.getDisguise((Player) sender);

                if (current != null) {

                    if (current.equals(Disguise.PLAYER)) {

                        sender.sendMessage(ChatColor.AQUA + "You are currently disguised as " + ChatColor.WHITE
                                + dm.getPlayerDisguiseName((Player) sender));

                    } else {

                        String friendlyName = dm.getDisguise((Player) sender).getFriendlyName();

                        sender.sendMessage(ChatColor.AQUA + "You are currently disguised as a" + ("aeiou"
                                .indexOf(friendlyName.toLowerCase().charAt(0)) == -1 ? " " : "n ")
                                + ChatColor.WHITE + friendlyName);

                    }
                }

                StringBuilder disguises = new StringBuilder(ChatColor.AQUA + "Available disguises: ");

                for (Disguise d : Disguise.values()) {

                    if (sender.hasPermission(d.getPermission())) {

                        disguises.append(ChatColor.WHITE);
                        disguises.append(d.getFriendlyName());
                        disguises.append(ChatColor.AQUA);
                        disguises.append(", ");

                    }
                }

                sender.sendMessage(disguises.toString());

            } else if (args.length == 1) {

                Disguise d = null;

                for (Disguise dis : Disguise.values()) {

                    if (dis.getFriendlyName().equalsIgnoreCase(args[0])) {

                        d = dis;
                        break;

                    }
                }

                if (d == null) {

                    sender.sendMessage(args[0] + ChatColor.RED + " is not a valid disguise.");

                } else {

                    if (sender.hasPermission(d.getPermission())) {

                        dm.disguise((Player) sender, d);
                        sender.sendMessage(ChatColor.AQUA + "You have been disguised as a" + ("aeiou"
                                .indexOf(d.getFriendlyName().toLowerCase().charAt(0)) == -1 ? " " : "n ")
                                + ChatColor.WHITE + d.getFriendlyName());

                    } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise as a "
                                + ChatColor.WHITE + d.getFriendlyName());
                }

            } else if (args.length == 2) {

                if (args[0].equalsIgnoreCase("player") || args[0].equalsIgnoreCase("p")) {

                    if (sender.hasPermission(Disguise.PLAYER.getPermission())) {

                        dm.disguisePlayer((Player) sender, args[1]);
                        sender.sendMessage(ChatColor.AQUA + "You have been disguised as " + ChatColor.WHITE + args[1]);

                    } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise as a "
                            + ChatColor.WHITE + Disguise.PLAYER.getFriendlyName());

                } else {

                    if (sender.hasPermission("kxdisguise.command.disguise.other")) {

                        Player p = Bukkit.getPlayer(args[1]);

                        if (p == null) {

                            sender.sendMessage(ChatColor.RED + "Player " + ChatColor.WHITE + args[1] + ChatColor.RED
                                    + " not found.");

                        } else {

                            Disguise d = null;

                            for (Disguise dis : Disguise.values()) {

                                if (dis.getFriendlyName().equalsIgnoreCase(args[0])) {

                                    d = dis;
                                    break;

                                }
                            }

                            if (d == null) {

                                sender.sendMessage(args[0] + ChatColor.RED + " is not a valid disguise.");

                            } else {

                                if (sender.hasPermission(d.getPermission())) {

                                    dm.disguise(p, d);
                                    sender.sendMessage(p.getName() + ChatColor.AQUA + " has been disguised as a" + ("aeiou"
                                            .indexOf(d.getFriendlyName().toLowerCase().charAt(0)) == -1 ? " " : "n ")
                                            + ChatColor.WHITE + d.getFriendlyName());
                                    p.sendMessage(ChatColor.AQUA + "You have been disguised as a" + ("aeiou"
                                            .indexOf(d.getFriendlyName().toLowerCase().charAt(0)) == -1 ? " " : "n ")
                                            + ChatColor.WHITE + d.getFriendlyName() + " by " + ChatColor.WHITE + sender
                                            .getName());

                                } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise as a "
                                        + ChatColor.WHITE + d.getFriendlyName());

                            }
                        }

                    } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise other players.");
                }

            } else {

                if (args[0].equalsIgnoreCase("player") || args[0].equalsIgnoreCase("p")) {

                    if (sender.hasPermission("kxdisguise.command.disguise.other")) {

                        Player p = Bukkit.getPlayer(args[2]);

                        if (p == null) {

                            sender.sendMessage(ChatColor.RED + "Player " + ChatColor.WHITE + args[2] + ChatColor.RED
                                    + " not found.");

                        } else {

                            if (sender.hasPermission(Disguise.PLAYER.getPermission())) {

                                dm.disguisePlayer(p, args[1]);
                                sender.sendMessage(ChatColor.AQUA + "You have been disguised as " + ChatColor.WHITE
                                        + args[1] + ChatColor.AQUA + " by " + ChatColor.WHITE + sender.getName());

                            } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise as a "
                                    + ChatColor.WHITE + Disguise.PLAYER.getFriendlyName());

                        }

                    } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise other players.");

                } else {

                    if (sender.hasPermission("kxdisguise.command.disguise.other")) {

                        Player p = Bukkit.getPlayer(args[1]);

                        if (p == null) {

                            sender.sendMessage(ChatColor.RED + "Player " + ChatColor.WHITE + args[1] + ChatColor.RED
                                    + " not found.");

                        } else {

                            Disguise d = null;

                            for (Disguise dis : Disguise.values()) {

                                if (dis.getFriendlyName().equalsIgnoreCase(args[0])) {

                                    d = dis;
                                    break;

                                }
                            }

                            if (d == null) {

                                sender.sendMessage(args[0] + ChatColor.RED + " is not a valid disguise.");

                            } else {

                                if (sender.hasPermission(d.getPermission())) {

                                    dm.disguise(p, d);
                                    sender.sendMessage(p.getName() + ChatColor.AQUA + " has been disguised as a" + ("aeiou"
                                            .indexOf(d.getFriendlyName().toLowerCase().charAt(0)) == -1 ? " " : "n ")
                                            + ChatColor.WHITE + d.getFriendlyName());
                                    p.sendMessage(ChatColor.AQUA + "You have been disguised as a" + ("aeiou"
                                            .indexOf(d.getFriendlyName().toLowerCase().charAt(0)) == -1 ? " " : "n ")
                                            + ChatColor.WHITE + d.getFriendlyName() + " by " + ChatColor.WHITE + sender
                                            .getName());

                                } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise as a "
                                        + ChatColor.WHITE + d.getFriendlyName());
                            }
                        }

                    } else sender.sendMessage(ChatColor.RED + "You do not have permission to disguise other players.");
                }

            }
            
        } else sender.sendMessage("This command can only be used by a player.");

        return true;
    }
}
