package com.kaixeleron.disguise.command;

import com.kaixeleron.disguise.engine.Disguise;
import com.kaixeleron.disguise.engine.DisguiseManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandUndisguise implements CommandExecutor {

    public CommandUndisguise(final DisguiseManager dm) {
        this.dm = dm;
    }

    private final DisguiseManager dm;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (sender instanceof Player) {

                Disguise d = dm.getDisguise((Player) sender);

                if (d == null) {

                    sender.sendMessage(ChatColor.RED + "You are not disguised.");

                } else {

                    dm.undisguise((Player) sender);
                    sender.sendMessage(ChatColor.AQUA + "You have been undisguised.");

                }

            } else sender.sendMessage(ChatColor.RED + "Console usage: " + ChatColor.RESET + label + " <Player>");

        } else {

            if (sender.hasPermission("kxdisguise.command.undisguise.other")) {

                Player p = Bukkit.getPlayer(args[0]);

                if (p == null) {

                    sender.sendMessage(ChatColor.RED + "Player " + ChatColor.WHITE
                            + args[0] + ChatColor.RED + " not found.");

                } else {

                    Disguise d = dm.getDisguise(p);

                    if (d == null) {

                        sender.sendMessage(p.getName() + ChatColor.RED + " is not disguised.");

                    } else {

                        dm.undisguise(p);
                        sender.sendMessage(p.getName() + ChatColor.AQUA + " has been undisguised.");
                        p.sendMessage(ChatColor.AQUA + "You have been undisguised by " + ChatColor.WHITE
                                + sender.getName());

                    }
                }

            } else sender.sendMessage(ChatColor.RED + "You do not have permission to undisguise other players.");
        }

        return true;
    }
}
